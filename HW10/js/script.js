"use strict"



const tabs = document.querySelector('.tabs');

tabs.addEventListener('click', (event) => {
const previasTab = document.querySelector('.tabs-title.active');
previasTab.classList.remove('active');
event.target.classList.add('active');

const data = event.target.getAttribute('data-text');
const previasText = document.querySelector(`.tabs-content-text.show`);
previasText.classList.remove('show');
const nextText = document.querySelector(`.tabs-content-text[data-text=${data}]`);
nextText.classList.add('show');

});

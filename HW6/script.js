   
"use strict"


const newUser = createNewUser();


function createNewUser() {

    const fName = prompt ("Ваше ім'я");
    const lName = prompt ("Ваше прізвище");
    const userBirthday = prompt ("Введіть дату народження у форматі dd.mm.yyyy");

    const newUser = {
                firstName: fName,
                lastName: lName,
                birthday: userBirthday,
                getLogin() {
                    return this.firstName.slice(0, 1).toLowerCase() + this.lastName.toLowerCase();
                },
                
                getAge(){
                    const now = new Date();
                    
                    function addZero(z) {
                       return (z < 10) ? '0' + z : z;
                    
                    }
                    
                    const today = (`${addZero(now.getMonth()) + 1}. ${addZero(now.getDate())}. ${now.getFullYear()}`);
                    const birthdayThisYear = (`${userBirthday.slice(3, 5)}. ${userBirthday.slice(0, 2)}. ${now.getFullYear()}`);
                    
                    let age;
                    age = now.getFullYear() - this.birthday.slice(6);
                    if (today < birthdayThisYear) {
                        age = age - 1; 
                    }

                    return age;

                },
                
                getPassword(){
                    return this.firstName.slice(0, 1).toUpperCase() + this.lastName.toLowerCase()+this.birthday.slice(6);
                },
        
            }
        
    return newUser;   
                     
};


console.log(`Функція createNewUser`, newUser);
console.log(`Функція getAge`, newUser.getAge());
console.log("Функція getPassword", newUser.getPassword());
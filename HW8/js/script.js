//1 Знайти всі параграфи на сторінці та встановити колір фону #ff0000

let allParagrafs = document.querySelectorAll('p');
console.log('Всі параграфи на сторінці', allParagrafs);


allParagrafs.forEach(allParagrafs => {
    allParagrafs.style.backgroundColor = '#ff0000';
})

//2 Знайти елемент із id="optionsList". Вивести у консоль.

let idElement = document.getElementById("optionsList");
console.log('Елемент із id="optionsList"',idElement);

//2 Знайти батьківський елемент та вивести в консоль. 

let parentNode = idElement.parentNode;
console.log('Батьківський елемент', parentNode);

//2 Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

let childNodes = idElement.childNodes;

console.log('Дочірні ноди', childNodes);

//2 Назви та тип нод

childNodes.forEach ( childNodes => {
    console.log('Назва', childNodes.nodeName);
    console.log('Тип', childNodes.nodeType);
}) 

//3 
let paragraf = document.getElementById('testParagraph');

paragraf.innerHTML = 'This is a paragraph';


//4 Отримати елементи вкладені в елемент із класом main-header і вивести їх у консоль. 
//Кожному з елементів присвоїти новий клас nav-item.

let mainHeaderElements = document.querySelector('.main-header');
console.log(mainHeaderElements);


let mainHeaderChildren = mainHeaderElements.children;

console.log(mainHeaderChildren);

let mainHeaderFirstElementChild = mainHeaderElements.firstElementChild;

let mainHeaderLastElementChild = mainHeaderElements.lastElementChild;

mainHeaderFirstElementChild.classList.add('nav-item');
console.log(mainHeaderFirstElementChild.classList);

mainHeaderLastElementChild.classList.add('nav-item');
console.log(mainHeaderLastElementChild.classList);


//5  Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

let sectionTitle = document.querySelectorAll('.section-title');
console.log(sectionTitle);

sectionTitle.forEach( sectionTitle => {
    sectionTitle.classList.remove('section-title');

});

console.log(sectionTitle);



"use strict";

const form = document.querySelector('.password-form');



form.addEventListener('click', (event) => {

   if (event.target.tagName === 'I') {
    if (event.target.classList.contains('fa-eye')) {
        event.target.classList.remove('fa-eye');
        event.target.classList.add('fa-eye-slash');

       const carentInput = event.target.previousElementSibling;
       carentInput.setAttribute('type', 'text');

    } else {
        event.target.classList.add('fa-eye');
        event.target.classList.remove('fa-eye-slash');

        const carentInput = event.target.previousElementSibling;
        carentInput.setAttribute('type', 'password');
    }
    
  };


});



form.addEventListener('submit', (event) =>{
    event.preventDefault();
    mistake();
    hidMistake();

});






function mistake() {
    if (form.querySelector('#first').value === form.querySelector('#second').value){
        alert ('You are welcome');
    } else {
        const mistake = document.querySelector('.mistake');
        mistake.style.display = 'block';
        
    }
};


function hidMistake() {
    const inputs = document.querySelectorAll('input');
    inputs.forEach((input) => {
        input.addEventListener('focus', () => {
            const mistake = document.querySelector('.mistake');
            mistake.style.display = 'none';
        })

    })
    
}